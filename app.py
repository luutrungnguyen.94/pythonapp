from flask import Flask
from redis import Redis


app = Flask(__name__)
redis=202

@app.route('/')
def hello():
 count = 123
 return 'Hello Nguyen Luu Trung! Welcome to my world! You have been seen {} time.\n'.format(count)

if __name__ == "__main__":
 app.run(host="0.0.0.0", port=8000, debug=True)

